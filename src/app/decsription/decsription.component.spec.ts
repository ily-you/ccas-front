import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DecsriptionComponent } from './decsription.component';

describe('DecsriptionComponent', () => {
  let component: DecsriptionComponent;
  let fixture: ComponentFixture<DecsriptionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DecsriptionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DecsriptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
