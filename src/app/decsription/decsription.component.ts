import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AnnoncesJsonld } from '../models/annonces-jsonld';

@Component({
  selector: 'app-decsription',
  templateUrl: './decsription.component.html',
  styleUrls: ['./decsription.component.scss']
})
export class DecsriptionComponent implements OnInit {

  public annonce: AnnoncesJsonld|null = null;


  constructor(
    private httpClient: HttpClient,
    private activatedRoute: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe((params) => {
 
      this.httpClient.get<AnnoncesJsonld>('http://localhost:8888/symfony/api/public/index.php/api/listings/' + params.id).subscribe({
        next: (annonce: AnnoncesJsonld) => {
          this.annonce = annonce;
        },
        error: (err: HttpErrorResponse) => {
   
          alert(err.status + ' - ' + err.statusText);
        },
      });
    });
  }
}
