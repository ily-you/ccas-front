import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { IndexComponent } from './index/index.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { DecsriptionComponent } from './decsription/decsription.component';
import { JumboComponent } from './jumbo/jumbo.component';
import { ContactComponent } from './contact/contact.component';
import { MentionsComponent } from './mentions/mentions.component';
import { PolitiquesComponent } from './politiques/politiques.component';
import { CguComponent } from './cgu/cgu.component';
import { ContactproComponent } from './contactpro/contactpro.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import {NgxPaginationModule} from 'ngx-pagination';
import { FormsModule } from '@angular/forms';
import { NgxSliderModule } from '@angular-slider/ngx-slider';
import { MatSliderModule } from '@angular/material/slider';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent,
    IndexComponent,
    HeaderComponent,
    FooterComponent,
    DecsriptionComponent,
    JumboComponent,
    ContactComponent,
    MentionsComponent,
    PolitiquesComponent,
    CguComponent,
    ContactproComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    Ng2SearchPipeModule,
    NgxPaginationModule,
    FormsModule,
    NgxSliderModule,
    MatSliderModule,
    BrowserAnimationsModule
 
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
