import { Component, OnInit } from '@angular/core';
import { AnnoncesCollection } from '../models/annonces-collection';
import { AnnoncesCollectionFilter } from '../models/annonces-collection-filter';
import { AnnoncesJsonld } from '../models/annonces-jsonld';
import {HttpClient} from '@angular/common/http';
import { Options, LabelType } from "@angular-slider/ngx-slider";

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent implements OnInit {
  public annonces: Array<AnnoncesJsonld> = [];
  myModel = 0;
  searchText = "";
  totalLength: any;
  page:number = 1;
  public prevLink: string|null = null;
  public nextLink: string|null = null;
  public lastPage:number|null = null;

  public filters: AnnoncesCollectionFilter = {
    title : '',
    fuel : '',
    brand: '',
    releaseYear: '',
    model: '',
    price: '',
    km: 0,
  };
  minPrice: number = 1000;   
  maxPrice: number = 180000;   
  price: Options = {     
    floor: 1000,     ceil: 180000 
  };
  minYear: number = 2000;   
  maxYear: number = 2021;   
  year: Options = {     
    floor: 2000,     ceil: 2021 
  };
  minKm: number = 1;   
  maxKm: number = 200000;   
  km: Options = {     
    floor: 1,     ceil: 200000
  };

  constructor(
    private httpClient: HttpClient,
  ) { }

  ngOnInit(): void {
    this.loadPage('/api/listings?page=1');
   
  }

  public applyFilters(page: number = 1): void {
    let url = '/api/listings?page=' + page;

    for (const key of Object.keys(this.filters)) {
      if (key in this.filters) {
        const val = this.filters[key as keyof AnnoncesCollectionFilter];

        if (val !== '') {
          url += '&' + key + '=' + val;
        }
        if(this.minPrice !== 0 && this.maxPrice !== 0) {
          url += '&price[between]=' + this.minPrice + '..' + this.maxPrice;
        }
        if(this.minYear !== 0 && this.maxYear !== 0) {
          url += '&releaseYear[between]=' + this.minYear + '..' + this.maxYear;
        }
        if(this.minKm !== 0 && this.maxKm !== 0) {
          url += '&km[between]=' + this.minKm + '..' + this.maxKm;
        }
      }
    }

    this.loadPage(url);
  }

    private loadPage(page: string): void {
   
      this.httpClient.get<AnnoncesCollection>('http://localhost:8888/symfony/api/public/index.php' + page).subscribe((data) => {
        this.annonces = data['hydra:member'];
  
        //if (data['hydra:view']['hydra:next'] === undefined ) {
          //this.nextLink = null;
        //}else{
        //this.nextLink = data['hydra:view']['hydra:next']
        //}
       // if (data['hydra:view']['hydra:previous'] === undefined ) {
       //   this.prevLink = null;
       // }else{
        //this.prevLink = data['hydra:view']['hydra:previous']
       // }
       // if (data['hydra:view']['hydra:last'] === undefined ) {
       //   this.lastPage = null;
       // }else{
        //  '/api/listings?page=1'
       // const regex = /\?.*page=([0-9]+)/; 
        //const str = data['hydra:view']['hydra:last'];
       // const matches = str.match(regex);
        //  if (matches === null){
       // this.lastPage = null;
       // }else{
      //  this.lastPage = parseInt(matches[1]);
       
       });
     }
   // public loadPreviousPage(): void {
    //  if (this.prevLink !== null){
     //   this.loadPage(this.prevLink);
    //  }
    //  }  
    //  public loadNextPage(): void {
     //   if (this.nextLink !== null){
     //     this.loadPage(this.nextLink);
     //   }
      //  }
      //  public loadPageByNumber(pageNumber: number):void {
      //    this.loadPage('/api/listings?page=' + pageNumber);
      //  }
      //  public get getPageNumbers(): Array<number> {
      //    const arr: Array<number> = [];
      //    if(this.lastPage !== null) {
      //      for(let i=1; i <= this.lastPage; i++){
      //        arr.push(i);
      //      }
      //    }
      //    return arr;
      //  }
}
