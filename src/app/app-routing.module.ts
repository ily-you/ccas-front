import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CguComponent } from './cgu/cgu.component';
import { ContactproComponent } from './contactpro/contactpro.component';
import { DecsriptionComponent } from './decsription/decsription.component';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { IndexComponent } from './index/index.component';
import { MentionsComponent } from './mentions/mentions.component';
import { PolitiquesComponent } from './politiques/politiques.component';

const routes: Routes = [
  {path: '', component: IndexComponent},
  {path: 'header', component: HeaderComponent},
  {path: 'footer', component: FooterComponent},
  {path: 'description/:id', component: DecsriptionComponent},
  {path: 'mentions', component: MentionsComponent},
  {path: 'politiques', component: PolitiquesComponent},
  {path: 'cgu', component: CguComponent},
  {path: 'contactpro', component: ContactproComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
