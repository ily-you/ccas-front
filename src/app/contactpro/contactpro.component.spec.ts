import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactproComponent } from './contactpro.component';

describe('ContactproComponent', () => {
  let component: ContactproComponent;
  let fixture: ComponentFixture<ContactproComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContactproComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactproComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
