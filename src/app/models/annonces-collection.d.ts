import { AnnoncesJsonld } from "./annonces-jsonld";

export interface AnnoncesCollection {
    'hydra:member': Array<AnnoncesJsonld>
    'hydra:totalItems': number;
    'hydra:view': {
        '@id': string;
        '@type': string;
        'hydra:first': string;
        'hydra:last': string;
        'hydra:next'?: string;
        'hydra:previous'?: string;
      };
}