export interface BrandJsonld {
    "@context": "string",
    "@id": "string",
    "@type": "string",
    "id": 0,
    marque: string;
}