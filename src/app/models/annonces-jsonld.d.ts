export interface AnnoncesJsonld {
    "@context": "string",
    "@id": "string",
    "@type": "string",
    "id": 0,
    title: string,
    description: string,
    descriptionAll: string,
    releaseYear: string,
    km: 0,
    price: string,
    brand: string,
    model: string,
    fuel: string,
    picture: string,
    garage: string
}
