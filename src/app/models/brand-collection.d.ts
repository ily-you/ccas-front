
import { BrandJsonld } from "./brand-jsonld";

export interface BrandCollection {
    'hydra:member': Array<BrandJsonld>
    'hydra:totalItems': number;
    'hydra:view': {
        '@id': string;
        '@type': string;
        'hydra:first': string;
        'hydra:last': string;
        'hydra:next'?: string;
        'hydra:previous'?: string;
      };
}