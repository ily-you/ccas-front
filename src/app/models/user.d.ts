export interface User {
    lastname: string;
    firstname: string;
    email: string;
    phone?: string;
    siren?: string;
    garages?: Array<string>;
}