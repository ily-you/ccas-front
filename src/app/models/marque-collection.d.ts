import { MarqueJsonld } from "./marque-jsonld";

export interface MarqueCollection {
    'hydra:member': Array<MarqueJsonld>
    'hydra:totalItems': number;
    'hydra:view': {
        '@id': string;
        '@type': string;
        'hydra:first': string;
        'hydra:last': string;
        'hydra:next'?: string;
        'hydra:previous'?: string;
      };
}