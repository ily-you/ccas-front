import { Options } from "@angular-slider/ngx-slider";

export interface AnnoncesCollectionFilter {
    title : string;
    fuel : string;
    brand : string;
    releaseYear: string;
    model: string;
    km: number;
    price: string;
}