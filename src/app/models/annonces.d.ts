export interface Annonces {
    title: string;
    releaseYear: string;
    km: 0;
    price: string;
    brand: string;
    model: string;
    fuel?: string;
    garage: string;
    description: string;
    descriptionAll:string;
    picture: string;
}