export interface MarqueJsonld {
    '@context': string;
    '@id': string;
    '@type': string;
    id: number;
    brand: string;
  }
  