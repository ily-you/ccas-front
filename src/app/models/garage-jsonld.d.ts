export interface GarageJsonld {
    "@context": "string",
  "@id": "string",
  "@type": "string",
  "id": 0,
    name: string;
    street: string;
    streetComplement: string;
    postalCode: number;
    city: string;
    owner: string;
    siret: string;
}