export interface UserJsonld {
    '@context': string;
    '@id': string;
    '@type': string;
    id: number;
    lastname: string;
    firstname: string;
    email: string;
    phone?: string;
    siren?: string;
    garages: Array<string>;
  }
  