import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { AnnoncesCollection } from 'src/app/models/annonces-collection';
import { AnnoncesCollectionFilter } from 'src/app/models/annonces-collection-filter';
import { AnnoncesJsonld } from 'src/app/models/annonces-jsonld';

@Component({
  selector: 'app-jumbo',
  templateUrl: './jumbo.component.html',
  styleUrls: ['./jumbo.component.scss']
})
export class JumboComponent implements OnInit {
  public annonces: Array<AnnoncesJsonld> = [];

  public prevLink: string|null = null;
  public nextLink: string|null = null;

  public lastPage:number|null = null;
  
  public filters: AnnoncesCollectionFilter = {
    fuel: '',
    title: '',
    brand: '',
    releaseYear: '',
    model: '',
    price: '',
    km: 0,
  };

  constructor(
    private httpClient: HttpClient,
  ) { }

  ngOnInit(): void {
   
  }

  public applyFilters(page: number = 1): void {
    let url = '/api/listings?page=' + page;

    for (const key of Object.keys(this.filters)) {
      if (key in this.filters) {
        const val = this.filters[key as keyof AnnoncesCollectionFilter];

        if (val !== '') {
          url += '&' + key + '=' + val;
        }
      }
    }

    this.loadPage(url);
  }

    private loadPage(page: string): void {
      this.httpClient.get<AnnoncesCollection>('http://localhost:8888/symfony/api/public/index.php' + page).subscribe((data) => {
        this.annonces = data['hydra:member'];
   
        if (data['hydra:view']['hydra:next'] === undefined ) {
          this.nextLink = null;
        }else{
        this.nextLink = data['hydra:view']['hydra:next']
        }
        if (data['hydra:view']['hydra:previous'] === undefined ) {
          this.prevLink = null;
        }else{
        this.prevLink = data['hydra:view']['hydra:previous']
        }
        if (data['hydra:view']['hydra:last'] === undefined ) {
          this.lastPage = null;
        }else{
          '/api/listings?page='
        const regex = /\?.*page=([0-9]+)/; 
        const str = data['hydra:view']['hydra:last'];
        const matches = str.match(regex);
if (matches === null){
  this.lastPage = null;
}else{
 this.lastPage = parseInt(matches[1]);
}
      
        }
       });
     }
      public loadNextPage(): void {
      if (this.nextLink !== null){
        this.loadPage(this.nextLink);
      }
      }
      public loadPreviousPage(): void {
      if (this.prevLink !== null){
        this.loadPage(this.prevLink);
      }
      }  
      public loadPageByNumber(pageNumber: number):void {
        this.applyFilters(pageNumber);
      }
      public get getPageNumbers(): Array<number> {
        const arr: Array<number> = [];
        if(this.lastPage !== null) {
          for(let i=1; i <= this.lastPage; i++){
            arr.push(i);
          }
        }
        return arr;
      }

}